from telegram.ext import Filters
import datetime
import pyowm
import config
import urllib
import json
from random import randint

class GenericPlugin():
    """ Generic plugin of telegram bot """
    enable = False

    def __init__(self, register_plugin):
        register_plugin(command_type='command', command='', callback=self.run, pass_args=False, pass_chat_data=False, pass_job_queue=False)

    def run(self, *args):
        pass

    def stop(self):
        pass

    def help(self):
        msg = ''
        return msg


class DateTime(GenericPlugin):
    """ Return the local time """

    enable = True

    def __init__(self, register_plugin):
        register_plugin(command_type='command', command='date', callback=self.run)

    def run(self, bot, update):
        bot.sendMessage(chat_id=update.message.chat_id, text=str(datetime.datetime.today()))

    def help(self):
        msg = '/date: Muestra la hora actual'
        return msg

class Weather(GenericPlugin):
    """ Return weather for especific City """

    enable = True

    def __init__(self, register_plugin):
        register_plugin(command_type='command', command='clima', callback=self.run, pass_args=True)
        self.owm = pyowm.OWM(config.apikey_owm)
        self.owm.set_language('es')

    def run(self, bot, update, args):
        if len(args) == 0:
            zone = 'Montevideo, UY'
        else:
            zone = ' '.join(args[:])
        print(str(datetime.datetime.today()),update.message.chat_id, 'pidio el clima para: ', zone)
        try:
            location = self.owm.weather_at_place(zone)
            loc = location.get_location()
            lat = loc.get_lat()
            lon = loc.get_lon()
            country = loc.get_country()
            city = loc.get_name()
            wheather = location.get_weather()
            status = wheather.get_detailed_status()
            temp = wheather.get_temperature('celsius')['temp']
            hum = wheather.get_humidity()
            press = wheather.get_pressure()['press']
            distv = wheather.get_visibility_distance()
            wind = wheather.get_wind(unit='meters_sec')
            vel_wind = wind['speed']
            if wind['deg'] > 315 and wind['deg'] <= 45:
                direc_wind = 'Norte'
            elif wind['deg'] > 45 and wind['deg'] <= 135:
                direc_wind = 'Este'
            elif wind['deg'] > 135 and wind['deg'] <= 285:
                direc_wind = 'Sur'
            else:
                direc_wind = 'Oeste'
            icon = wheather.get_weather_icon_url()
            msg = 'El clima se presenta con %s en %s, %s\nLat: %.2f, Long: %.2f\n' % (status, city, country, lat, lon)
            msg = msg + 'Temperatura: %.1f grados \n' % temp if temp is not None else msg
            msg = msg + 'Humedad: %.2f %s\n' % (hum, '%') if hum is not None else msg
            msg = msg + 'Presión: %.1f hPa\n' % press if press is not None else msg
            msg = msg + 'Visibilidad: %.3f Kilometros\n' % (distv / 1000) if distv is not None and distv != 0 else msg
            msg = msg + 'Vientos del %s, a una velocidad de %.1f Km/h\n' % (direc_wind, vel_wind * 3.6) if vel_wind is not None else msg
            # bot.sendPhoto(chat_id=update.message.chat_id, photo=icon, caption=msg)
            bot.sendPhoto(chat_id=update.message.chat_id, photo=icon)
            bot.sendMessage(chat_id=update.message.chat_id, text=msg)
        except:
            bot.sendMessage(chat_id=update.message.chat_id, text='Zona %s no encontrada' % zone)

    def help(self):
        msg = "/clima [<lugar>]: Muestra el clima de un lugar, si no se coloca mostrara 'Montevideo, UY'"
        return msg

class Xkcd(GenericPlugin):
    """ Generic plugin of telegram bot """
    enable = True

    def __init__(self,  register_plugin):
        register_plugin(command_type='command', command='xkcd', callback=self.run, pass_args=True)

    def run(self, bot, update, args):
        try:
            print(str(datetime.datetime.today()),update.message.chat_id, 'pidio xkcd')
            data = urllib.request.urlopen("http://xkcd.com/info.0.json").read()
            j = json.loads(data.decode('utf8'))
            max_num = j['num']
            num = max_num
            if len(args) == 1 and args[0] == 'random':
                num = randint(1, max_num)
                data = urllib.request.urlopen("http://xkcd.com/%i/info.0.json" % num).read()
                j = json.loads(data.decode('utf8'))
            bot.sendPhoto(chat_id=update.message.chat_id, photo=j['img'], caption='xkcd id: %i' % num)
        except:
            bot.sendMessage(chat_id=update.message.chat_id, text="No se pudo descargar la última imagen de xkcd")

    def help(self):
        msg =  '/xkcd [random]: Parametro opcional: random, si se pasa '
        msg += '                el parémotro se elije un comic al azar,\n'
        msg += '                en otro caso envía el último chiste de xkcd'
        return msg


class Listener(GenericPlugin):
    """ Listener of all message recived, matched by the filter """

    enable = True

    def __init__(self, register_plugin):
        register_plugin(command_type='listener', filters=Filters.text | Filters.contact, callback=self.run)

    def run(self, bot, update):
        chat_id = update.message.chat_id
        msg = ''
        if update.message.text is not None:
            msg += update.message.text
        if update.message.contact is not None:
            msg += ' ' + str(update.message.contact)
        print(str(datetime.datetime.today()), chat_id, msg)


class Alarm(GenericPlugin):
    """ Create delayed messages beta state only global 1 alarm """
    enable = True

    def __init__(self, register_plugin):
        register_plugin(command_type='command', command='alarmon', callback=self.alarmon, pass_args=True, pass_job_queue=True, pass_chat_data=True)
        register_plugin(command_type='command', command='alarmoff', callback=self.alarmoff, pass_chat_data=True)
        self._text = ''

    def alarmon(self, bot , update, args, job_queue, chat_data):
        chat_id = update.message.chat_id
        try:
            if args[0][-1] is 's':
                time = int(args[0])
            elif args[0][-1] is 'm':
                time = int(args[0][0:-1]) * 60
            elif args[0][-1] is 'h':
                time = int(args[0][0:-1]) * 60 * 60
            else:
                time = int(args[0])
            self._text = ' '.join(args[1:]) if len(args) > 1 else 'alarma'
            print(str(datetime.datetime.today()),update.message.chat_id, 'prendio la alrma', time, self._text)
            if time <= 0:
                update.message.reply_text("No se especifico un tiempo en segundos positivo")
                return
            job = job_queue.run_once(self.alarm, time, context=chat_id)
            chat_data['job'] = job
            update.message.reply_text('La alarma será enviada en %i segundos' % time)
        except:
            update.message.reply_text("Error de párametros usar:\n/alarmon <tiempo>[<unidad>] <mensaje>")

    def alarmoff(self, bot, update, chat_data):
        print(str(datetime.datetime.today()),update.message.chat_id, 'apago la alrma')
        if 'job' not in chat_data:
            update.message.reply_text('No hay tarea programada')
        else:
            job = chat_data['job']
            job.schedule_removal()
            del chat_data['job']
            update.message.reply_text('Alarma apagada!')
        return

    def alarm(self, bot, job):
        bot.sendMessage(job.context, text=self._text)

    def help(self):
        msg = "/alarmon <tiempo>[<unidad>] <mensaje>: Envia el <mensaje> luego de <tiempo> segundos.\n"
        msg += "    Se puede usar <unidad> puede ser:"
        msg += "    s:segundos , m:minutos, h:horas, d:días, si se omite se consideran segundos.\n"
        msg += "    Estado Beta (1 sola alarma global)\n\n"
        msg += "/alarmoff: Apaga la alarma"
        return msg

class Forkme(GenericPlugin):
    """ Forkme URL """
    enable = True

    def __init__(self, register_plugin):
        register_plugin(command_type='command', command='forkme', callback=self.run)

    def run(self, bot, update):
        msg = ('Forkme in: https://gitlab.com/dzipitria/franclinbot-telegram')
        bot.sendMessage(chat_id=update.message.chat_id, text=msg)

    def help(self):
        msg = '/forkme: Return Forkme URL'
        return msg
