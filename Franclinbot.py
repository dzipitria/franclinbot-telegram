import telegram
#TODO: Sacar Filters del import de abajo
from  telegram.ext import Filters
from telegram.ext import CommandHandler, MessageHandler, Updater
import config
import datetime
import plugins
import inspect
import sys

class Bot():
    """ A telegram bot """
    def __init__(self):
        self.bot = telegram.Bot(token=config.token)
        self.bot_updater = Updater(self.bot.token)
        self.commands_help = ''

        self.register_plugin(command_type='command', command='start',callback=self.start)
        self.addHelp('/start: Inicializa tus opciones personales.')
        #Autostart all class into plugins have enable = True
        plugins_auto_enabled = []
        for m in inspect.getmembers(plugins, inspect.isclass):
            try:
                if m[1].__module__ == 'plugins' and m[1].enable:
                    plugins_auto_enabled.append(m[1])
            except:
                pass
        self.plugins_enabled = []
        for m in plugins_auto_enabled:
            self.addPlugIn(m)
            # obj = m(self.register_plugin)
            # self.plugins_enabled.append(obj)
            # self.addHelp(obj.help())
        self.register_plugin(command_type='command', command='help', callback=self.help)
        self.addHelp('/help: Muestra esta ayuda')

    def addPlugIn(self, plugin_class):
        obj = plugin_class(self.register_plugin)
        self.plugins_enabled.append(obj)
        self.addHelp(obj.help())
        
    def register_plugin(self, command_type, **kargs):
        if command_type == 'command':
            print("agregando comando", kargs['command'])
            start_handler = CommandHandler(**kargs)
        elif command_type == 'listener':
            print("agregando listener", kargs['filters'])
            start_handler = MessageHandler(**kargs)
        else:
            print("Error commando: %s no soportado" % command_type)
            sys.exit(-1)
        self.bot_updater.dispatcher.add_handler(start_handler)
        print('agregado !!')

    def start(self, bot, update):
        bot.sendMessage(chat_id=update.message.chat_id, text="Hola mundo!\n\n ¿Que puedes hacer con este bot?")
        self.help(bot, update)

    def stop(self):
        for p in self.plugins_enabled:
            p.stop()
        self.bot.sendMessage(chat_id=update.message.chat_id, text="Bye!")
        #TODO: Do exit!!

    def addHelp(self, msg):
        if msg != '':
            if self.commands_help == '':
                self.commands_help = msg
            else:
                self.commands_help += '\n\n' + msg

    def help(self, bot, update):
        print(str(datetime.datetime.today()),update.message.chat_id, 'pidio ayuda')
        if len(self.commands_help) > 0:
            bot.sendMessage(chat_id=update.message.chat_id, text=self.commands_help)
        else:
            bot.sendMessage(chat_id=update.message.chat_id, text='No help')

    def run(self):
        self.bot_updater.start_polling()
        self.bot_updater.idle()


bot = Bot()
bot.run()
