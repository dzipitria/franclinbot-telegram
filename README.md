**Franclinbot-Telegram**

Is a Bot implemented in Python 3.5.3 using the python-telegram-bot library.

Currently you can find it on telegram searching @Franclinbot.

It is easily extensible to using plugins (plugins.py), currently there are some plugins implemented.
Simply create your class by inheriting the GenericPlugin class

At present Franclin.py is almost in production.
The plugin as Alarm works but is in alpha state.
He is currently in the telegram groups:
     https://t.me/GNULinuxUY
     https://t.me/debian_es_irc

Have fun!

**First**

cp config.new.py config.py

Edit config.py and set the variables token and apikey_owm
